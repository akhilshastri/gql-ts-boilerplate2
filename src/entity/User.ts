import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm';
import { randomBytes, pbkdf2 } from 'crypto';
import { ObjectType, Field, ID, Root } from 'type-graphql';
import { IsEmailAlreadyExist } from '#root/utils/user/emailExistsConstraint';

@ObjectType()
@Entity()
export class User extends BaseEntity {
  static getSalt() {
    return randomBytes(16).toString('hex');
  }

  static genHash(password: string, salt: string): Promise<string> {
    return new Promise((resolve, reject) => {
      pbkdf2(password, salt, 20, 64, 'sha512', (err, derivedKey) => {
        if (err) {
          reject(err);
        } else {
          resolve(derivedKey.toString('hex'));
        }
      });
    });
  }

  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @Column()
  firstName: string;

  @Field()
  @Column()
  lastName: string;

  @Field()
  @Column('text', { unique: true })
  @IsEmailAlreadyExist({ message: 'Email already exists' })
  email: string;

  @Column()
  password: string;

  @Column()
  salt: string;

  @Column('bool', { default: false })
  confirmed: boolean;

  @Field()
  fullName(@Root() user: User): string {
    return `${user.firstName} ${user.lastName}`;
  }

  validatePassword(password: string) {
    return new Promise((resolve, reject) => {
      pbkdf2(password, this.salt, 20, 64, 'sha512', (err, derivedKey) => {
        if (err) {
          reject(err);
        } else {
          resolve(derivedKey.toString('hex') === this.password);
        }
      });
    });
  }


}
