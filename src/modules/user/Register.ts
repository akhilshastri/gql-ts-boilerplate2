import { Resolver, Query, Mutation, Arg, UseMiddleware, Authorized } from 'type-graphql';
import { User } from '#root/entity/User';
import { RegisterInput } from './inputTypes/RegisterInput';
import { isAuth } from '../middleware/isAuth';
import { logger } from '#root/modules/middleware/logger';
import { sendMail } from '#root/utils/sendMail';
import { createTokenUrl } from '#root/utils/createTokenUrl';

@Resolver(() => User)
export class RegisterResolver {
  @UseMiddleware(isAuth, logger)
  @Authorized()
  @Query(() => String, { name: 'about', })
  async helloWorld() {
    return "GraphQL TS Boilerplate";
  }

  @Mutation(() => User)
  async register(
    @Arg('input') { email, firstName, lastName, password }: RegisterInput,
  ): Promise<User> {
    const salt = User.getSalt();
    const hashPass = await User.genHash(password, salt);
    const user = await User.create({ firstName, lastName, email, salt, password: hashPass }).save();


    await sendMail(email, await createTokenUrl(user.id, 'user/confirm'));

    return user;
  }

  // @FieldResolver(() => String)
  // fullName(@Root() parent: User): string {
  //   return `${parent.firstName} ${parent.lastName}`;
  // }

}
