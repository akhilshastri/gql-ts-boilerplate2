import { Resolver, Mutation, Arg, Query } from 'type-graphql';
import { User } from '../../entity/User';
import { redis } from '#root/utils/redis';

@Resolver()
export class ConfirmUserResolver {
  @Query(() => String, { name: 'about', })
  async about() {
    return "GraphQL TS Boilerplate";
  }
  @Mutation(() => Boolean)
  async confirmUser(@Arg('token') token: string): Promise<boolean> {
    const userId = await redis.get(token);
    if (!userId) {
      return false;
    }

    await User.update({ id: userId }, { confirmed: true });
    await redis.del(token);
    return true;
  }

}
