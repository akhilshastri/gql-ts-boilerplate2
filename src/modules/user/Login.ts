import { Resolver, Mutation, Arg, Ctx, Query } from 'type-graphql';
import { User } from '../../entity/User';
import { MyContext } from '#root/types/MyContext';

@Resolver()
export class LoginResolver {
  @Query(() => String, { name: 'about', })
  async about() {
    return "GraphQL TS Boilerplate";
  }
  @Mutation(() => User, { nullable: true })
  async login(
    @Arg("email") email: string,
    @Arg("password") password: string,
    @Ctx() ctx: MyContext
  ): Promise<User | null> {
    const user = await User.findOne({ where: { email } });

    if (!user) {
      return null;
    }

    const validUser = await user.validatePassword(password);
    if (!validUser || !user.confirmed) {
      return null;
    }

    ctx.req.session!.userId = user.id;

    return user;
  }

}
