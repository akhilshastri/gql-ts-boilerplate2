import { Resolver, Mutation, Arg } from 'type-graphql';

import { User } from '#root/entity/User';
import { createTokenUrl } from '#root/utils/createTokenUrl';
import { sendMail } from '#root/utils/sendMail';

@Resolver()
export class ForgotPasswordResolver {
  @Mutation(() => Boolean)
  async forgotPassword(@Arg('email') email: string): Promise<boolean> {
    const user = await User.findOne({ where: { email } });
    if (!user) {
      return false;
    }

    await sendMail(email, await createTokenUrl(user.email, 'user/change-password'));
    return true;
  }
}
