import { Resolver, Mutation, Arg } from 'type-graphql';

import { User } from '#root/entity/User';
import { ChangePasswordInput } from './inputTypes/ChangePasswordInput';
import { redis } from '#root/utils/redis';

@Resolver()
export class ChangePasswordResolver {
  @Mutation(() => User, { nullable: true })
  async changePassword(@Arg('data') { token, password }: ChangePasswordInput): Promise<User | null> {
    const userId = await redis.get(token);

    if (!userId) {
      return null;
    }

    const user = await User.findOne(userId);
    if (!user) {
      return null;
    }

    user.password = await User.genHash(password, user.salt);
    await user.save();
    await redis.del(token);
    return user;
  }
}
