import { InputType, Field } from 'type-graphql';
import { Length, IsEmail } from 'class-validator';

@InputType()
export class RegisterInput {
  @Field()
  @Length(1, 60)
  firstName: string;

  @Field()
  @Length(1, 60)
  lastName: string;

  @Field()
  @IsEmail()
  @Length(1, 60)
  email: string;

  @Field()
  @Length(8, 60)
  password: string;
}
