import { MiddlewareFn } from 'type-graphql';
import { MyContext } from '#root/types/MyContext';

export const isAuth: MiddlewareFn<MyContext> = async ({context: {req}}, next) => {
  if (!req.session!.userId) {
    throw new Error ('Access Denied: You must be authenticated to access this resource')
  }
  return next();
};
