import dotenv from 'dotenv';
dotenv.config();
import 'reflect-metadata';
import 'module-alias/register';
import { ApolloServer } from "apollo-server-express";
import Express from "express";
import { buildSchema } from "type-graphql";
import { createTypeormConn } from './utils/createTypeOrmConn';
import session from 'express-session';
import connectRedis from 'connect-redis';
import cors from 'cors';

import { redis } from '#root/utils/redis';
import { LoginResolver } from './modules/user/Login';
import { RegisterResolver } from './modules/user/Register';
import { MeResolver } from './modules/user/Me';
import { ConfirmUserResolver } from './modules/user/ConfirmUser';
import { ForgotPasswordResolver } from './modules/user/ForgotPassword';
import { ChangePasswordResolver } from './modules/user/ChangePassword';

const main = async () => {
  await createTypeormConn();

  let schema;
  try {
    schema = await buildSchema({
      resolvers: [RegisterResolver, LoginResolver, MeResolver, ConfirmUserResolver, ForgotPasswordResolver, ChangePasswordResolver],
      authChecker: ({ context: { req } }
        // {root, args, context, info}, roles
      ) => {
        return !!req.session.userId;
      }
    });
  } catch (e) {
    console.error(e);
    throw e;
  }


  const apolloServer = new ApolloServer({ schema, context: ({ req }: any) => ({ req }) });

  const app = Express();

  const RedisStore = connectRedis(session);

  app.use(cors({ credentials: true, origin: 'http://localhost:3000' }));
  app.use(
    session({
      store: new RedisStore({
        client: redis as any  // TODO: fix this any type to RedisStoreOption
      }),
      name: "gql-bp2",
      secret: process.env.SERVER_SECRET as string,
      resave: false,
      saveUninitialized: false,
      cookie: {
        httpOnly: true,
        secure: process.env.NODE_ENV === "production",
        maxAge: 1000 * 60 * 60 * 24 * 30 // 30 days
      }
    })
  );
  apolloServer.applyMiddleware({ app });

  app.listen(4000, () => {
    console.log("server started on http://localhost:4000/graphql");
  });
};

main();
