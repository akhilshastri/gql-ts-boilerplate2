import { redis } from '#root/utils/redis';
import { v4 } from 'uuid';

export const createTokenUrl = async (id: string, path: string): Promise<string> => {
  const token = v4();
  await redis.set(token, id, 'ex', 60 * 60 * 24); // 1 day
  return `http://localhost:3000/${path}/${token}`;
};
