import nodemailer from 'nodemailer';

export async function sendMail(email: string, url: string) {
  const account = await nodemailer.createTestAccount();

  const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false,
    auth: {
      user: account.user,
      pass: account.pass
    }
  });

  const mailOptions = {
    from: '"Fred Fredly" <fred@fre.net>',
    to: email,
    subject: "BP Test: Confirm email address",
    text: "hello world!",
    html: `<a href="${url}">${url}</a>`
  };

  const info = await transporter.sendMail(mailOptions);

  console.log(`Message Sent: ${info.messageId}`);
  console.log(`Preview Url: ${nodemailer.getTestMessageUrl(info)}`);
}
