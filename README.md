# GraphQL Typescript Boilerplate

## Database setup

PostGRES database: gql-bp-2 
PG Port (dev): 7998

docker run `-p 0.0.0.0:7998:5432`
--name pg-db `
-e POSTGRES_PASSWORD=password`
-e POSTGRES_USER=gql-dev `
-e POSTGRES_DB=gql-react`
-e POSTGRES_PORT=5432 `
-d postgres:12.1-alpine

## Redis Setup

docker run `
-p 0.0.0.0:7997:6379 `
--name devRedis `
-e REDIS_PASSWORD=password `
-d redis:alpine `
--appendonly yes
